import React from 'react';
import { state } from './state';

function App() {
  const renderColumns = () => {
    return state.map((column) => {
      /** Функция, которая рендерит JSX-колонок.
       * В React часто используется функция map.
       * Она помогает нам взять обычные "данные" и превратить их в JSX-элементы
       */
    });
  };

  //Мы можем вызывать функции в JSX
  //Вообще, любые выражения (expressions) допустимы в рамках JSX-кода
  //Например: вызов функции, тернарный оператор -- являются выражениями
  //А вот объявление переменной let x = 10 -- выражением не является, оно является инструкцией (statements)
  //Статья про различия выражений и инструкций -- https://medium.com/launch-school/javascript-expressions-and-statements-4d32ac9c0e74
  return <div>{renderColumns()}</div>;
}

export default App;
