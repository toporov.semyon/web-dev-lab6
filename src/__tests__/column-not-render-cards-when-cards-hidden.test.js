import { it, expect } from '@jest/globals';
import { render } from '@testing-library/react';
import { Column } from '../components';

it(`Компонент Column не рендерит карточки, когда передан пропс isCardsHidden`, () => {
  const columnName = 'TODO';
  const cards = [
    { id: 1, name: 'Покормить кота' },
    { id: 2, name: 'Купить хлеб' },
  ];

  (() => {
    const { queryByText } = render(
      <Column name={columnName} cards={cards} isCardsHidden={true} />
    );

    cards.forEach((card) => {
      const isCardMounted = queryByText(card.name);
      expect(isCardMounted).toBeFalsy();
    });
  })();

  (() => {
    const { queryByText } = render(
      <Column name={columnName} cards={cards} isCardsHidden={false} />
    );

    cards.forEach((card) => {
      const isCardMounted = queryByText(card.name);
      expect(isCardMounted).toBeTruthy();
    });
  })();
});
