import { it, expect } from '@jest/globals';
import { render } from '@testing-library/react';
import { Column } from '../components';

it(`Компонент Column должен рендерить все карточки,
переданные в пропсах с помощью компонента Card`, () => {
  const columnName = 'TODO';
  const cards = [
    { id: 1, name: 'Покормить кота' },
    { id: 2, name: 'Купить хлеб' },
  ];

  const { getByText } = render(<Column name={columnName} cards={cards} />);

  cards.forEach((card) => {
    const isCardMounted = getByText(card.name);
    expect(isCardMounted).toBeTruthy();
  });
});
